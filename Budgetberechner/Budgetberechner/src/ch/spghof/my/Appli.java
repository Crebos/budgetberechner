package ch.spghof.my;

/*
 * @Author Yves Meyer
 * @Date 29.09.2019
 * @Version 3.2
 * 
 */

import java.io.IOException;

import ch.spghof.my.model.Account;
import ch.spghof.my.view.MainGuiController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Appli extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Account acc = new Account();
		
		try {
			
			//FXML Loading
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MainGui.fxml"));
		
			AnchorPane rootPane = loader.load();
			
			MainGuiController mgController = loader.getController();
			mgController.setAccount(acc);
			mgController.fillTemplatePositions();
			mgController.setStage(primaryStage);
			
			//Scene setup
			Scene scene = new Scene(rootPane);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Budgetberechner");
			primaryStage.show();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
		
	}

}
