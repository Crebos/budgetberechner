package ch.spghof.my.model;

import java.util.ArrayList;
import ch.spghof.my.model.calc.CalcMonth;
import ch.spghof.my.model.calc.CalcYear;
import ch.spghof.my.model.time.Month;
import ch.spghof.my.model.time.Year;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class Account {
	
	private ObservableList<Position> positions;
	private ArrayList<Year> years;
	private CalcYear cy;
	private CalcMonth cm;
	
	//Constructor
	public Account() {
		
		positions = FXCollections.observableArrayList();
		positions.addListener((ListChangeListener.Change<? extends Position> change) -> {
			while(change.next()) {
				if(change.wasUpdated() || change.wasAdded() || change.wasRemoved() || change.wasReplaced())
					resetSavedYears();
			}
		});
		
		years = new ArrayList<>();
		cy = new CalcYear(positions);
		cm = new CalcMonth(positions);
		
	}
	
	//File Saving
	public void SaveToFile(String path) {
		
		//Todo
		
	}
	
	//Print
	public void togglePrint(boolean b) {
		cy.togglePrint(b);
		cm.togglePrint(b);
	}
	
	/* Information Interfaces
	 * 
	 * Wenn man eine Zahlung auf 00.2020 erstellt wird diese f�r den FOLGENDEN MONAT berechnet
	 * also wird die Zahlung so behandelt als w�rde sie irgendwann im ersten Monat abgerechnet werden (00.2020 - 01.2020)
	 * 
	 */
	public double getValueOfMonth(int y, int m) {
		double value = 0;
		
		value = getMonth(getYear(y), m).getSaldo();
		
		return value;
	}
	
	public double getValueOfYear(int y) {
		double value = 0;
		
		value = getYear(y).getSaldo();
		
		return value;
	}
	
	public void resetSavedYears() {
		years.clear();
	}
	
	//Checking if year exists -> if not: create
	public Year getYear(int yn) {
		Year year = null;
		
		for(Year y : years) {
			if(y.getYear() == yn)
				year = y;
		}
		
		if(year == null) {
			year = cy.createYear(yn);
			years.add(year);
		}
		
		return year;
		
	}
	
	//Checking if month exists -> if not: create
	public Month getMonth(Year y, int mn) {
		
		Month month = null;
		
		for(Month m : y.getMonthsList()) {
			if(m.getMonth() == mn)
				month = m;
		}
		
		if(month == null) {
			month = cm.createMonth(y.getYear(), mn);
			y.setMonth(month);
		}
		
		return month;
		
	}
	
	//Operators for Positions
	public void addPosition(Position p) {
		positions.add(p);
	}
	
	public void removePosition(Position p) {
		positions.remove(p);
	}
	
	public ObservableList<Position> getPositionsList() {
		return positions;
	}

}
