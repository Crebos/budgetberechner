package ch.spghof.my.model.time;

import java.util.ArrayList;

public class Year {
	
	private ArrayList<Month> months;
	private double saldo;
	private int year;
	
	//Constructor
	public Year(int year, double saldo) {
		months = new ArrayList<>();
		this.year = year;
		this.saldo = saldo;
	}
	
	//Getters & setters
	public int getYear() {
		return year;
	}
	
	public ArrayList<Month> getMonthsList() {
		return months;
	}
	
	public Month getMonth(int m) {
		return months.get(m);
	}
	
	public void setMonth(Month m) {
		months.add(m);
	}
	
	public double getSaldo() {
		return saldo;
	}

}
