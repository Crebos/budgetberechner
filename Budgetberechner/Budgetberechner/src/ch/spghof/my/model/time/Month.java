package ch.spghof.my.model.time;

public class Month {
	
	private double saldo;
	private int month;
	
	//Constructor
	public Month(int month, double saldo) {
		this.month = month;
		this.saldo = saldo;
	}
	
	//Getters
	public int getMonth() {
		return month;
	}
	
	public double getSaldo() {
		return saldo;
	}

}
