package ch.spghof.my.model;

import ch.spghof.my.util.Date;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Position {
	
	private StringProperty name, dateRange;
	private Date startDate, endDate;
	private IntegerProperty intervalType;
	private DoubleProperty value;
	
	/* IntervalType definition
	 * 0 = once
	 * 1 = monthly
	 * 2 = every 2 month
	 * 3 = every 3 month
	 * ...
	 * 
	 * 12 = every year
	 * 13 = every year and 1 month
	 * ...
	 * 
	 */
	
	//Constructors
	public Position(String name, Date startDate, int intervalType, double value) {
		
		this.name = new SimpleStringProperty(name);
		this.startDate = startDate;
		this.intervalType = new SimpleIntegerProperty(intervalType);
		this.value = new SimpleDoubleProperty(value);
		
		this.dateRange = new SimpleStringProperty(startDate.toString());
		
	}
	
	public Position(String name, Date startDate, Date endDate, int intervalType, double value) {
		
		this.name = new SimpleStringProperty(name);
		this.startDate = startDate;
		this.endDate = endDate;
		this.intervalType = new SimpleIntegerProperty(intervalType);
		this.value = new SimpleDoubleProperty(value);
		
		this.dateRange = new SimpleStringProperty(startDate.toString() + " ; " + endDate.toString());
		
	}
	
	//Name Getters & setters
	public StringProperty NameProperty() {
		return name;
	}
	
	public String getName() {
		return name.get();
	}
	
	public void setName(String s) {
		name.set(s);
	}
	
	//Date Getters & setters
	public StringProperty dateRangeProperty() {
		return dateRange;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date d) {
		startDate = d;
		this.dateRange.set(startDate.toString());
		
	}

	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date d) {
		endDate = d;
		this.dateRange.set(startDate.toString() + " ; " + endDate.toString());
		
	}
	
	//IntervalType Getters & setters
	public IntegerProperty intervalTypeProperty() {
		return intervalType;
	}

	public int getIntervalType() {
		return intervalType.get();
	}
	
	public void setIntervalType(int i) {
		intervalType.set(i);
	}
	
	//Value Getters & setters
	public DoubleProperty valueProperty() {
		return value;
	}

	public double getValue() {
		return value.get();
	}
	
	public void setValue(double d) {
		value.set(d);
	}
	
}
