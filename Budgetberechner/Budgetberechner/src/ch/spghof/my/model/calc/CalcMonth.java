package ch.spghof.my.model.calc;

import ch.spghof.my.model.Position;
import ch.spghof.my.model.time.Month;
import javafx.collections.ObservableList;

public class CalcMonth {
	
	private ObservableList<Position> positions;
	private boolean print = false;
	
	//Constructor
	public CalcMonth(ObservableList<Position> positions) {
		this.positions = positions;
	}
	
	//Creation of Month
	public Month createMonth(int y, int m) {
		
		Month month = new Month(m, calcSaldo(y, m));
		
		return month;
		
	}
	
	//Algorithmus f�r Saldo
	private double calcSaldo(int y, int m) {
		double saldo = 0;
		int rt = 0, demandTime = (y * 12) + m;
		
		for(int i = 0; i < positions.size(); i++) {
			
			//Berechnung Laufzeit in Monaten
			if(positions.get(i).getEndDate() == null || demandTime <= positions.get(i).getEndDate().getDateInMonths()) {
				rt = demandTime - positions.get(i).getStartDate().getDateInMonths();
			}
			else {
				rt = positions.get(i).getEndDate().getDateInMonths() - positions.get(i).getStartDate().getDateInMonths();
			}
			
			//Berechnung Anzahl vollst�ndiger Intervals
			int intervalTimes = 0;
			if(rt > 0) {
				if(positions.get(i).getIntervalType() != 0)
					intervalTimes = rt / positions.get(i).getIntervalType();
				else
					intervalTimes = 1;
			}
			
			//Berechnung Saldo
			saldo = saldo + (intervalTimes * positions.get(i).getValue());
			
			if(print)
				printData(i, rt, intervalTimes, saldo, demandTime);
		}
		
		return saldo;
	}
	
	//Print Funktionen
	private void printData(int i, int rt, int intervalTimes, double saldo, int demandTime) {
		
		System.out.println("Position Nr " + i + " (" + positions.get(i).getName() + ")" + " wird berechnet ..");
		
		System.out.println("StartDate: " + positions.get(i).getStartDate().getDateInMonths() + " DemandedTime: " + demandTime);
		System.out.println("RunTime: " + rt + " IntervalType: " + positions.get(i).getIntervalType() + " IntervalTimes: " + intervalTimes);
		System.out.println("Saldo new: " + saldo + " Change in Saldo: " + (intervalTimes * positions.get(i).getValue()) + " Value per Interval: " + positions.get(i).getValue());
		System.out.println("----------");
		
	}
	
	public void togglePrint(boolean b) {
		print = b;
	}

}
