package ch.spghof.my.model.calc;

import ch.spghof.my.model.Position;
import ch.spghof.my.model.time.Year;
import javafx.collections.ObservableList;

public class CalcYear {
	
	private ObservableList<Position> positions;
	private boolean print = false;
	
	//Constructor
	public CalcYear(ObservableList<Position> positions) {
		this.positions = positions;
	}
	
	//Creation of Year
	public Year createYear(int y) {
		
		Year year = new Year(y, calcSaldo(y));
		
		return year;
	}
	
	
	//Algorithmus for Saldo
	private double calcSaldo(int y) {
		double saldo = 0;
		int rt = 0, demandTime = (y * 12);
		
		for(int i = 0; i < positions.size(); i++) {
			
			//Berechnung Laufzeit
			if(positions.get(i).getEndDate() == null || demandTime <= positions.get(i).getEndDate().getDateInMonths()) {
				rt = demandTime - positions.get(i).getStartDate().getDateInMonths();
			}
			else {
				rt = positions.get(i).getEndDate().getDateInMonths() - positions.get(i).getStartDate().getDateInMonths();
			}
			
			//Berechnung Anzahl vollständiger Intervals
			int intervalTimes = 0;
			if(rt > 0) {
				if(positions.get(i).getIntervalType() != 0)
					intervalTimes = rt / positions.get(i).getIntervalType();
				else
					intervalTimes = 1;
			}
			
			//Berechnung Saldo
			saldo = saldo + (intervalTimes * positions.get(i).getValue());
			
			if(print)
				printData(i, rt, intervalTimes, saldo, demandTime);
			
		}
		
		return saldo;
	}
	
	//Print Funktionen
	private void printData(int i, int rt, int intervalTimes, double saldo, int demandTime) {
		
		System.out.println("Position Nr " + i + " (" + positions.get(i).getName() + ")" + " wird berechnet ..");
		
		System.out.println("StartDate: " + positions.get(i).getStartDate().getDateInMonths() + " DemandedTime: " + demandTime);
		System.out.println("RunTime: " + rt + " IntervalType: " + positions.get(i).getIntervalType() + " IntervalTimes: " + intervalTimes);
		System.out.println("Saldo new: " + saldo + " Value per Interval: " + positions.get(i).getValue());
		System.out.println("----------");
		
	}
	
	public void togglePrint(boolean b) {
		print = b;
	}

}
