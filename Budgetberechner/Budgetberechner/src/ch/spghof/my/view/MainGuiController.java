package ch.spghof.my.view;

import ch.spghof.my.model.Account;
import ch.spghof.my.model.Position;
import ch.spghof.my.util.Date;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainGuiController {
	
	//Positions List & Columns
	@FXML
	private TableView<Position> positionsList;
	@FXML
	private TableColumn<Position, String> tcName;
	@FXML
	private TableColumn<Position, String> tcStartEnd;
	@FXML
	private TableColumn<Position, Integer> tcInterval;
	@FXML
	private TableColumn<Position, Double> tcValue;
	
	//LineChart Diagram
	@FXML
	private LineChart<Number, Number> lc;
	private XYChart.Series<Number, Number> series;
	
	//TextFields
	@FXML
	private TextField tfYear;
	@FXML
	private TextField tfMonth;
	@FXML
	private TextField tfDiagram;
	
	//Labels
	@FXML
	private Label lbYearSaldo;
	@FXML
	private Label lbMonthSaldo;
	@FXML
	private Label lbDiagramm;
	
	//Konto / Model / Scene
	private Account activeAcc;
	private Stage myStage;
	
	//Setup 
	public void setAccount(Account acc) {
		activeAcc = acc;
		
		//Linechart setup
		series = new XYChart.Series<>();
		series.setName("Konto");
		lc.getData().add(series);
		
		
		//TableColumn Setup
		tcName.setCellValueFactory(new PropertyValueFactory<Position, String>("name"));
		tcStartEnd.setCellValueFactory(new PropertyValueFactory<Position, String>("dateRange"));
		tcInterval.setCellValueFactory(new PropertyValueFactory<Position, Integer>("intervalType"));
		tcValue.setCellValueFactory(new PropertyValueFactory<Position, Double>("value"));
		
		positionsList.setItems(acc.getPositionsList());
		
	}
	
	public void setStage(Stage s) {
		myStage = s;
	}
	
	//Calculate Button Functions
	@FXML
	private void calcYear() {
		
		int yint = -1;
		String ystring = tfYear.getText();
		if(ystring.matches("^([0-9]{4})$"))
			yint = Integer.parseInt(ystring);
		
		if(yint >= 0)
			lbYearSaldo.setText("Saldo: " + activeAcc.getValueOfYear(yint) + " CHF");
		else {
			lbYearSaldo.setText("Invalid Input");
			Alert alert = new Alert(AlertType.WARNING, "Ihre Eingabe ist ung�ltig", ButtonType.OK);
			alert.showAndWait();
		}
			
	}
	
	@FXML
	private void calcMonth() {
		
		int yint = -1, mint = -1;
		Date d;
		String input = tfMonth.getText();
		if(input.matches("^[0-9]{2}.{1}[0-9]{4}") && !input.contains(",")) { //Bug mit Komma in Input entdeckt -> deshalb extra �berpr�fung
			d = new Date(input);
			yint = d.getYear();
			mint = d.getMonth();
		}
			
		
		if(yint >= 0 && mint >= 0)
			lbMonthSaldo.setText("Saldo: " + activeAcc.getValueOfMonth(yint, mint) + " CHF");
		else {
			lbMonthSaldo.setText("Invalid Input");
			Alert alert = new Alert(AlertType.WARNING, "Ihre Eingabe ist ung�ltig", ButtonType.OK);
			alert.showAndWait();
		}
			
		
	}
	
	@FXML
	private void calcDiagram() {
		
		series.getData().clear();
		
		int yint = -1;
		String ystring = tfDiagram.getText();
		if(ystring.matches("^([0-9]{4})$"))
			yint = Integer.parseInt(ystring);
		
		if(yint >= 0) {
			for(int i = 0; i <= 12; i++) {
				series.getData().add(new XYChart.Data<Number, Number>(i, activeAcc.getValueOfMonth(yint, i)));
			}
			lbDiagramm.setText("success");
		}
		else {
			lbDiagramm.setText("Invalid Input");
			Alert alert = new Alert(AlertType.WARNING, "Ihre Eingabe ist ung�ltig", ButtonType.OK);
			alert.showAndWait();
		}
		
	}
	
	//Menu Button Functions
	@FXML
	public void fillTemplatePositions() {
		
		if(activeAcc != null) {
			
			activeAcc.addPosition(new Position("StartKap", new Date(2020, 0), 0, 2000));
			activeAcc.addPosition(new Position("Einkommen", new Date(2020, 0), 1, 1800));
			
			activeAcc.addPosition(new Position("Wohnen", new Date(2020, 0), 1, -800));
			activeAcc.addPosition(new Position("Sozialversicherungen", new Date(2020, 0), 1, -120));
			
			activeAcc.addPosition(new Position("Training Tennis", new Date(2020, 6), 6, -950));
			activeAcc.addPosition(new Position("Training Kampfsport", new Date(2020, 0), 12, -1600));
			activeAcc.addPosition(new Position("Essensgeld", new Date(2020, 0), 1, -300));
			activeAcc.addPosition(new Position("Server kosten", new Date(2020, 0), 1, -40));
			activeAcc.addPosition(new Position("HandyAbbo", new Date(2020, 0), 1, -60));
			
		}
	}
	
	@FXML
	public void resetPositions() {
		activeAcc.getPositionsList().clear();
	}
	
	@FXML
	public void closeProgram() {
		myStage.close();
	}
	
	@FXML
	public void openAbout() {
		Alert alert = new Alert(AlertType.INFORMATION, "Dieses Programm wurde von Yves Meyer entwickelt. Er besucht zurzeit die Klasse 3I in der IMS und mag programmieren. \nDanke f�r das ausgiebige Testen :)", ButtonType.FINISH);
		alert.show();
	}
	
	@FXML
	public void enablePrint() {
		activeAcc.togglePrint(true);
	}
	
	@FXML
	public void disablePrint() {
		activeAcc.togglePrint(false);
	}
	
	//Positions Button Functions
	@FXML
	private void editPosition() {
		
		Position p = positionsList.getSelectionModel().getSelectedItem();
		
		if(p != null)
			try {
				
				//FXML Loading
				FXMLLoader loader = new FXMLLoader(getClass().getResource("PositionEditor.fxml"));
				
				Stage stage = new Stage();
				AnchorPane pane = loader.load();
				
				PositionEditorController controller = loader.getController();
				controller.setPosition(p);
				controller.setStage(stage);
				
				stage.setScene(new Scene(pane));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setTitle("Position Editor");
				stage.showAndWait();
				
				if(controller.getPosition() != null)
					p = controller.getPosition();
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		else {
			Alert alert = new Alert(AlertType.WARNING, "Bitte w�hlen Sie zuerst einen Eintrag aus, um ihn zu bearbeiten!", ButtonType.OK);
			alert.showAndWait();
		}
			
		
	}
	
	@FXML
	private void deletePosition() {
		
		Position p = positionsList.getSelectionModel().getSelectedItem();
		
		if(p != null)
			activeAcc.removePosition(p);
		
		else {
			Alert alert = new Alert(AlertType.WARNING, "Bitte w�hlen Sie zuerst einen Eintrag aus, um ihn zu l�schen!", ButtonType.OK);
			alert.showAndWait();
		}
		
	}
	
	@FXML
	private void newPosition() {
		
		try {
			
			//FXML Loading
			FXMLLoader loader = new FXMLLoader(getClass().getResource("PositionEditor.fxml"));
			
			Stage stage = new Stage();
			AnchorPane pane = loader.load();
			
			PositionEditorController controller = loader.getController();
			controller.setStage(stage);
			
			stage.setScene(new Scene(pane));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setTitle("Position Editor");
			stage.showAndWait();
			
			if(controller.getPosition() != null)
				activeAcc.addPosition(controller.getPosition());
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
