package ch.spghof.my.view;

import ch.spghof.my.model.Position;
import ch.spghof.my.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class PositionEditorController {
	
	//TextFields
	@FXML
	private TextField tfName;
	@FXML
	private TextField tfStartDate;
	@FXML
	private TextField tfEndDate;
	@FXML
	private TextField tfValue;
	@FXML
	private TextField tfInterval;
	
	//Model and Stage
	private Position position;
	private Stage stage;
	
	public void setPosition(Position p) {
		position = p;
		
		tfName.setText(p.getName());
		tfStartDate.setText(p.getStartDate().toString());
		if(p.getEndDate() != null)
			tfEndDate.setText(p.getEndDate().toString());
		tfValue.setText("" + p.getValue());
		tfInterval.setText("" + p.getIntervalType());
		
	}
	
	public void setStage(Stage s) {
		stage = s;
	}
	
	//Buttons
	public void save() {
		
		boolean correct = true;
		String name = tfName.getText();
		String startDateString = tfStartDate.getText();
		Date startDate = null;
		String endDateString = tfEndDate.getText();
		Date endDate = null;
		String valueString = tfValue.getText();
		double value = 0;
		String intervalTypeString = tfInterval.getText();
		int intervalType = 0;
		
		if((endDateString.matches("^[0-9]{2}.{1}[0-9]{4}") && !startDateString.contains(","))) //Bug mit Komma in Input entdeckt -> deshalb extra überprüfung
			endDate = new Date(endDateString);
		else
			correct = false;
		
		if (endDateString.equals("")) {
			endDate = null;
			correct = true;
		}
		
		if(startDateString.matches("^[0-9]{2}.{1}[0-9]{4}") && !startDateString.contains(",")) //Bug mit Komma in Input entdeckt -> deshalb extra überprüfung
			startDate = new Date(startDateString);
		else
			correct = false;
		
		if(valueString.matches("^[-+]?\\d+(\\.{0,1}(\\d+?))?$"))
			value = Double.parseDouble(valueString);
		else
			correct = false;
		
		if(intervalTypeString.matches("^-?\\d+$"))
			intervalType = Integer.parseInt(intervalTypeString);
		else
			correct = false;
		
		if(correct) {
			if(position == null) {
				if(endDate == null)
					position = new Position(name, startDate, intervalType, value);
				else
					position = new Position(name, startDate, endDate, intervalType, value);
			}
			else {
				position.setName(name);
				position.setStartDate(startDate);
				if(endDate != null)
					position.setEndDate(endDate);
				position.setValue(value);
				position.setIntervalType(intervalType);
			}
			stage.close();
			
		}
		else {
			Alert alert = new Alert(AlertType.WARNING, "Ihre Eingabe ist ungültig", ButtonType.OK);
			alert.showAndWait();
		}
		
	}
	
	public void cancel() {
		
		stage.close();
		
	}
	
	public Position getPosition() {
		return position;
	}
	

}
