package ch.spghof.my.util;

public class Date {
	
	private int year, month;
	
	//Constructors
	public Date(int year, int month) {
		
		this.year = year;
		this.month = month;
		
		while(month >= 12) {
			year++;
			month = month - 12;
		}
		
	}
	
	public Date(String input) {
		
		this.year = Integer.parseInt(input.substring(input.indexOf('.')+1, input.length()));
		this.month = Integer.parseInt(input.substring(0, input.indexOf('.')));
		
		while(month >= 12) {
			year++;
			month = month - 12;
		}
		
	}
	
	//Getters
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDateInMonths() {
		
		int m = (year * 12) + month;
		
		return m;
	}
	
	@Override
	public String toString() {
		
		String m, y, s;
		if(month < 10)
			m = "0" + month;
		else
			m = "" + month;
		
		if(year < 10)
			y = "000" + year;
		else if(year < 100)
			y = "00" + year;
		else if(year < 1000)
			y = "0" + year;
		else
			y = "" + year;
		
		s = m + "." + y;
		
		return s;
	}

}
