package ch.kbw.am.model;

import java.util.ArrayList;
import java.util.Collections;

import ch.kbw.am.algo.Algorithm;
import ch.kbw.am.model.util.ObjektCombiRankComparator;
import ch.kbw.am.model.util.ObjektCombiValueComparator;

public class CalculationData {
	
	private ArrayList<ObjektCombination> combinations;
	private ArrayList<Objekt> objects;
	private ObjektCombinationCalculator cc;
	private String name, algoName;
	private double maxWeight;
	private Model model;
	private float time;
	
	public CalculationData(String name, String algoName, double maxWeight, Model model) {
		
		this.name = name;
		this.algoName = algoName;
		this.maxWeight = maxWeight;
		combinations = new ArrayList<>();
		objects = new ArrayList<>();
		cc = new ObjektCombinationCalculator(objects);
		this.model = model;
		
		System.out.println("Calculation Data " + name + " has been created");
		
	}
	
	public CalculationData() {
		
	}
	
	//Initate calculation
	public void calculate() throws Exception {
		
		Algorithm algo = model.getAlgorithmFromName(algoName, this);
		float startTime = 0;
		float endTime = 0;
		System.out.println("MaxWeight: " + maxWeight);
		System.out.println("Number of Objects: " + objects.size());
		
		//Wenn alles richtig ist, Algo starten sonst Fehlerausgabe
		if(algo != null) {
			if(maxWeight > 0) {
				if(objects.size() > 0) {
					System.out.println("Algorithm " + algoName + " is starting...");
					startTime = System.nanoTime();
					algo.start();
				}
				else
					throw new Exception("Invalid Objekts Input!");
			}
			else
				throw new Exception("Invalid maximum weight!");
		}
		else
			throw new Exception("Invalid Algorithm!");
		
		//sorting and ranking Object Combinations
		Collections.sort(combinations, new ObjektCombiValueComparator());
		
		int rankCounter = 1;
		for (ObjektCombination oc : combinations) {
			if(oc.getTotalWeight() <= maxWeight) {
				oc.setRank(rankCounter);
				rankCounter++;
			}
			else
				oc.setRank(-1);
		}

		combinations.sort(new ObjektCombiRankComparator());
		
		endTime = System.nanoTime();
		time = ((endTime - startTime)/1000000000);
		
	}
	
	//Combinations operations
	public ObjektCombination createCombi(String combination) {
		
		cc.setCombination(combination);
		double totalWeight = cc.getTotalWeight();
		double totalValue = cc.getTotalValue();
		
		ObjektCombination oc = new ObjektCombination(combination, totalWeight, totalValue);
		
		combinations.add(oc);
		
		return oc;
		
	}
	
	
	//Object operations
	public void addObjects(Objekt object) {
		objects.add(object);
	}
	
	
	//variables operations	
	public ArrayList<Objekt> getObjects() {
		return objects;
	}
	
	public ArrayList<ObjektCombination> getCombinations() {
		return combinations;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAlgoName() {
		return algoName;
	}
	
	public double getMaxWeight() {
		return maxWeight;
	}
	
	public double getTime() {
		return time;
	}
	
	public void setObjects(ArrayList<Objekt> objects) {
		this.objects = objects;
	}
	
	public void setCombinations(ArrayList<ObjektCombination> combinations) {
		this.combinations = combinations;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAlgoName(String algoName) {
		this.algoName = algoName;
	}

	public void setMaxWeight(double maxWeight) {
		this.maxWeight = maxWeight;
	}

	public void setTime(float time) {
		this.time = time;
	}
	
	public int getNObjects() {
		return objects.size();
	}
	

}
