package ch.kbw.am.model.wrapper;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.kbw.am.model.CalculationData;

@XmlRootElement(name = "calculationDataList")
public class CalculationDataListWrapper {
	
	private List<CalculationData> calculationDataList;
	
	@XmlElement(name = "CalculationData")
	public List<CalculationData> getObjektList() {
		return calculationDataList;
	}
	
	public void setObjektList(List<CalculationData> cd) {
		calculationDataList = cd;
		System.out.println("Objekt List Added" );
	}

}
