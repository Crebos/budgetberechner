package ch.kbw.am.model.wrapper;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ch.kbw.am.model.Objekt;

@XmlRootElement(name = "objektList")
public class ObjektListWrapper {
	
	private List<Objekt> objektList;
	
	@XmlElement(name = "Objekt")
	public List<Objekt> getObjektList() {
		return objektList;
	}
	
	public void setObjektList(List<Objekt> o) {
		objektList = o;
	}

}
