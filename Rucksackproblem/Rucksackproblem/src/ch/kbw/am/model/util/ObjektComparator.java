package ch.kbw.am.model.util;

import java.util.Comparator;

import ch.kbw.am.model.Objekt;

public class ObjektComparator implements Comparator<Objekt> {

	@Override
	public int compare(Objekt o1, Objekt o2) {
		int comp = 0;
		
		if(o1.getProfit() < o2.getProfit())
			comp = 1;
		if(o1.getProfit() > o2.getProfit())	
			comp = -1;
		
		return comp;
	}
	

}
