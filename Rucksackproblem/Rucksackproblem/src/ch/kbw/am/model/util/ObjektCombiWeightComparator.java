package ch.kbw.am.model.util;

import java.util.Comparator;

import ch.kbw.am.model.ObjektCombination;

public class ObjektCombiWeightComparator implements Comparator<ObjektCombination> {

	@Override
	public int compare(ObjektCombination oc1, ObjektCombination oc2) {
		
		int comp = 0;
		if(oc1.getTotalWeight() < oc2.getTotalWeight())
			comp = 1;
		if(oc1.getTotalWeight() > oc2.getTotalWeight())
			comp = -1;
		
		return comp;
	}
	

}
