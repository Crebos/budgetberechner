package ch.kbw.am.model.util;

import java.util.Comparator;

import ch.kbw.am.model.ObjektCombination;

public class ObjektCombiRankComparator implements Comparator<ObjektCombination> {

	@Override
	public int compare(ObjektCombination oc1, ObjektCombination oc2) {
		
		int comp = 0;
		if(oc1.getRank() != -1 && oc2.getRank() != -1) {
			if(oc1.getRank() > oc2.getRank())
				comp = 1;
			if(oc1.getRank() < oc2.getRank())
				comp = -1;
		}	
			
		if(oc1.getRank() == -1 && oc2.getRank() > 0)
			comp = 1;
		
		if(oc2.getRank() == -1 && oc1.getRank() > 0)
			comp = -1;
		
		return comp;
	}
	

}
