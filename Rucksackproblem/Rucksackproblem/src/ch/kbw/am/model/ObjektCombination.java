package ch.kbw.am.model;

public class ObjektCombination {
	
	private String combination;
	private double totalWeight, totalValue;
	private int rank = -1;
	
	public ObjektCombination(String combination, double totalWeight, double totalValue) {
		
		this.combination = combination;
		this.totalWeight = totalWeight;
		this.totalValue = totalValue;
		
	}
	
	public ObjektCombination() {
		
	}
	
	public String getCombination() {
		return combination;
	}
	
	public double getTotalWeight() {
		return totalWeight;
	}
	
	public double getTotalValue() {
		return totalValue;
	}

	public int getRank() {
		return rank;
	}

	public void setCombination(String combination) {
		this.combination = combination;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public void setTotalValue(double totalValue) {
		this.totalValue = totalValue;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	

}
