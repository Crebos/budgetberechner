package ch.kbw.am.model;

public class Objekt {
	
	private String name;
	private double weight, value, profit;
	
	public Objekt(String name, double weight, double value) {
		
		this.name = name;
		this.weight = weight;
		this.value = value;
		this.profit = (value / weight);		
	}
	
	public Objekt() {
		
	}
	
	public String getName() {
		return name;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public double getValue() {
		return value;
	}
	
	public double getProfit() {
		return profit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWeight(double weight) {
		this.weight = weight;
		this.profit = (value / weight);
	}

	public void setValue(double value) {
		this.value = value;
		this.profit = (value / weight);
	}
	
	public void setProfit(double p) {
		profit = p;
	}
	
}
