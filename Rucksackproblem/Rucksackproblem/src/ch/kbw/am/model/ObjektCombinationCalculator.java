package ch.kbw.am.model;

import java.util.ArrayList;

public class ObjektCombinationCalculator {
	
	private ArrayList<Objekt> allObjectsList;
	private ArrayList<Objekt> combiObjectList;
	
	public ObjektCombinationCalculator(ArrayList<Objekt> list) {
		
		this.allObjectsList = list;
		combiObjectList = new ArrayList<>();
		
	}
	
	public void setCombination(String combination) {
		
		//Konfigurierung des combination Codes
		combiObjectList.clear();
		for (int i = 0; i < combination.length(); i++) {
			if(combination.charAt(i) == '1')
				combiObjectList.add(allObjectsList.get(i));			
		}
	}
	
	public double getTotalWeight() {
		double weight = 0;
		
		for(int i = 0; i < combiObjectList.size(); i++) {
			weight = weight + combiObjectList.get(i).getWeight();
		}
		
		return weight;
	}
	
	public double getTotalValue() {
		double value = 0;
		
		for(int i = 0; i < combiObjectList.size(); i++) {
			value = value + combiObjectList.get(i).getValue();
		}
		
		return value;
	}

}
