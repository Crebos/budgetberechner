package ch.kbw.am.model;

import java.util.ArrayList;

import ch.kbw.am.algo.Algorithm;
import ch.kbw.am.algo.BruteForce;
import ch.kbw.am.algo.ParetoOptimal;
import ch.kbw.am.algo.YuanAlgo;

public class Model {
	
	private ArrayList<CalculationData> calculationDataList;
	private String[] algoTypesName;
	
	public Model() {
		
		calculationDataList = new ArrayList<>();
		algoTypesName = new String[3];
		algoTypesName[0] = "Bruteforce";
		algoTypesName[1] = "Pareto-optimal";
		algoTypesName[2] = "Yuan Algo";
		
	}
	
	public void addData(CalculationData d) {
		calculationDataList.add(d);
		
	}
	
	public ArrayList<CalculationData> getCalculationDataList() {
		return calculationDataList;
	}
	
	public String[] getAlgoTypesName() {
		return algoTypesName;
	}
	
	public Algorithm getAlgorithmFromName(String s, CalculationData cd) {
		Algorithm algo = null;
		
		if(s != null)
			switch(s) {
			
			case "Bruteforce":
				algo = new BruteForce(cd);
				break;
				
			case "Pareto-optimal":
				algo = new ParetoOptimal(cd);
				break;
				
			case "Yuan Algo":
				algo = new YuanAlgo(cd);
				break;
			
			}
		
		return algo;
	}
	
	
}
