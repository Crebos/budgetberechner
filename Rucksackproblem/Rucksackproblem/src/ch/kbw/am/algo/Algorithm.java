package ch.kbw.am.algo;

import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Objekt;

public abstract class Algorithm {
	
	protected CalculationData cd;
	
	
	public Algorithm(CalculationData cd) {
		this.cd = cd;
	}
	
	public abstract void start();
	
	protected String createString(Objekt... objekts) {
		
		System.out.println("Creating String of Objects");
		
		String s = "";
		for(int i = 0; i < cd.getObjects().size(); i++) {
			for (Objekt objekt : objekts) {
				if(objekt == cd.getObjects().get(i))
					s = s.concat("1");
			}
			if(s.length() == i) {
				s = s.concat("0");
			}
				
		}
		
		System.out.println("generated String: " + s);
		return s;
		
	}
	
	protected String createStringAdditional(String oldComb, Objekt objekt) {
		
		System.out.println("Adding objekt called " + objekt.getName() + " to String: " + oldComb);
		String s = "";
		
		for(int i = 0; i < cd.getObjects().size(); i++) {
			if(objekt == cd.getObjects().get(i))
				s = s.concat("1");
			if(s.length() == i)
				s = s.concat(oldComb.charAt(i) + "");
		}
		
		System.out.println("generated String: " + s);
		return s;
	}

}
