package ch.kbw.am.algo;

import java.util.ArrayList;

import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Objekt;
import ch.kbw.am.model.ObjektCombination;
import ch.kbw.am.model.util.ObjektCombiWeightComparator;

public class ParetoOptimal extends Algorithm {
	
	ArrayList<ArrayList<ObjektCombination>> lists;

	public ParetoOptimal(CalculationData cd) {
		super(cd);
	}

	@Override
	public void start() {
		
		System.out.println("Starting calculations ... ");
		System.out.println("Will calculate " + cd.getObjects().size() + " Objekts");
		lists = new ArrayList<>();
		lists.add(0, new ArrayList<>());
		Objekt tempobj = null;
		System.out.println("Adding 0 to pareto-optimal solutions");
		lists.get(0).add(cd.createCombi(createString(tempobj)));
		
		for(int i = 0; i < (cd.getObjects().size()); i++) {
			System.out.println("### Adding Objekt " + i + " to Pareto-optimal solutions ###");
			
			ArrayList<ObjektCombination> tempList = new ArrayList<>();
			for(int j = 0; j < lists.get(i).size(); j++) {
				System.out.println("Creating new Combination Solution for combination " + j);
				tempList.add(cd.createCombi(createStringAdditional(lists.get(i).get(j).getCombination(), cd.getObjects().get(i))));
			}
			
			System.out.println("Starting to merge old and new List");
			lists.add(mergeList(lists.get(i), tempList));
			
			System.out.println("########## End of objekt " + i + " adding ##########");
		}
		
	}
	
	private ArrayList<ObjektCombination> mergeList(ArrayList<ObjektCombination> listOne, ArrayList<ObjektCombination> listTwo) {
		
		listOne.sort(new ObjektCombiWeightComparator());
		listTwo.sort(new ObjektCombiWeightComparator());
		
		ArrayList<ObjektCombination> ocList = new ArrayList<>();
		double pmax = -1;
		int counter = 0;
		boolean retry = true;
		ObjektCombination oc1 = null;
		ObjektCombination oc2 = null;
		
		System.out.println("Entering merging-loop");
		while(retry) {
			System.out.println(counter + " times in loop ... pmax is: " + pmax);
			
			oc1 = null;
			oc2 = null;
			int i = 0;
			System.out.println("Searching in listOne");	
			while(oc1 == null & i < listOne.size()) {
				if(listOne.get(i).getTotalValue() > pmax)
					oc1 = listOne.get(i);
				else
					i++;
			}
			i = 0;
			System.out.println("Searching in listTwo");
			while(oc2 == null & i < listTwo.size()) {
				if(listTwo.get(i).getTotalValue() > pmax)
					oc2 = listTwo.get(i);
				else
					i++;
			}
			if(oc1 == null) {
				System.out.println("No matching combi for listOne found");
				for (ObjektCombination objektCombi : listTwo) {
					if(objektCombi.getTotalValue() > pmax)
						ocList.add(objektCombi);
				}
				retry = false;
			}
			if(oc2 == null) {
				System.out.println("No matching combi for listTwo found");
				for (ObjektCombination objektCombi : listOne) {
					if(objektCombi.getTotalValue() > pmax)
						ocList.add(objektCombi);
				}
				retry = false;
			}
			if(retry)
				if(oc1.getTotalWeight() < oc2.getTotalWeight() | oc1.getTotalWeight() == oc2.getTotalWeight()
						& oc1.getTotalValue() > oc2.getTotalValue()) {
					System.out.println("matching combi for listOne found");
					ocList.add(oc1);
					pmax = oc1.getTotalValue();
				}
				else {
					System.out.println("matching combi for listTwofound");
					ocList.add(oc2);
					pmax = oc2.getTotalValue();
				}
				
			counter++;
			System.out.println("going to retry: " + retry);
		}
		
		System.out.println("left merging-loop");
		return ocList;
		
	}

}
