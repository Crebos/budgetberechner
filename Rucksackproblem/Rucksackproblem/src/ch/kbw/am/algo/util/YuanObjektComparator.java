package ch.kbw.am.algo.util;

import java.util.Comparator;

public class YuanObjektComparator implements Comparator<YuanObjekt> {

	@Override
	public int compare(YuanObjekt yo1, YuanObjekt yo2) {
		
		int comp = 0;
		if(yo1.getWeightProfit() < yo2.getWeightProfit())
			comp = 1;
		if(yo1.getWeightProfit() > yo2.getWeightProfit())
			comp = -1;
		
		return comp;
	}

}
