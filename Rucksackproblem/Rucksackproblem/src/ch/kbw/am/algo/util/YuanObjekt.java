package ch.kbw.am.algo.util;

import ch.kbw.am.model.Objekt;

public class YuanObjekt extends Objekt {
	
	private int objektIndex;
	private double weightProfit;
	
	public YuanObjekt(String name, double weight, double value, double weightProfit, int objektIndex) {
		super(name, weight, value);
		this.weightProfit = weightProfit;
		this.objektIndex = objektIndex;
		
	}

	public double getWeightProfit() {
		return weightProfit;
	}

	public void setWeightProfit(double weightProfit) {
		this.weightProfit = weightProfit;
	}

	public int getObjektIndex() {
		return objektIndex;
	}

	public void setObjektIndex(int objektIndex) {
		this.objektIndex = objektIndex;
	}
	
	

}
