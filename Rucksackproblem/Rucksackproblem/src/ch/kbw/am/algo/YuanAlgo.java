package ch.kbw.am.algo;

import java.util.ArrayList;

import ch.kbw.am.algo.util.YuanObjekt;
import ch.kbw.am.algo.util.YuanObjektComparator;
import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Objekt;
import ch.kbw.am.model.ObjektCombination;

public class YuanAlgo extends Algorithm {
	
	ArrayList<YuanObjekt> yuanObjekts;
	
	public YuanAlgo(CalculationData cd) {
		super(cd);
		
		yuanObjekts = new ArrayList<>();
		for (Objekt o : cd.getObjects()) {
			double gMax = o.getWeight()/cd.getMaxWeight();
			yuanObjekts.add(new YuanObjekt(o.getName(), o.getWeight(), o.getValue(), gMax*o.getProfit(), cd.getObjects().indexOf(o)));
		}
		
	}

	@Override
	public void start() {
		
		yuanObjekts.sort(new YuanObjektComparator());
		System.out.println(yuanObjekts.size());
		
		Objekt tempObj = null;
		ObjektCombination oc = cd.createCombi(createString(tempObj));
		for (YuanObjekt o : yuanObjekts) {
			ObjektCombination tempOc = cd.createCombi(createStringAdditional(oc.getCombination(), o));
			if(tempOc.getTotalWeight() <= cd.getMaxWeight())
				oc = tempOc;
			System.out.println("New generated String : " + oc.getCombination());
		}
		
	}
	
	protected String createStringAdditional(String oldComb, YuanObjekt objekt) {
		
		System.out.println("Adding objekt called " + objekt.getName() + " to String: " + oldComb);
		String s = "";
		
		for(int i = 0; i < cd.getObjects().size(); i++) {
			if(cd.getObjects().get(yuanObjekts.indexOf(objekt)) == cd.getObjects().get(i))
				s = s.concat("1");
			if(s.length() == i)
				s = s.concat(oldComb.charAt(i) + "");
		}
		
		System.out.println("generated String: " + s);
		return s;
	}

}
