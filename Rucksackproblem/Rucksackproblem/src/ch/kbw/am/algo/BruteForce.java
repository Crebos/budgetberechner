package ch.kbw.am.algo;

import ch.kbw.am.model.CalculationData;

public class BruteForce extends Algorithm {

	public BruteForce(CalculationData cd) {
		super(cd);
	}

	@Override
	public void start() {
		String combi = "";
		int n = cd.getNObjects();
		
		calc(combi, n);
	}
	
	private void calc(String combi, int n) {
		
		if(n == 0) {
			cd.createCombi(combi);
			System.out.println("Combination " + combi + " created!");
		}
		else {
			for(int i = 0; i < 2; i++) {
				if(i == 0)
					calc(combi.concat("0"), n-1);
				else 
					if(i == 1)
						calc(combi.concat("1"), n-1);
			}	
		}
	}
	

}