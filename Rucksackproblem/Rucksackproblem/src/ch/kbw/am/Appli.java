package ch.kbw.am;

/*
 * @author Yves Meyer
 * @version 2.2
 */

import ch.kbw.am.model.Model;
import ch.kbw.am.view.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Appli extends Application {
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		try {
			
			System.out.println("starting...");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MainView.fxml"));
			
			BorderPane rootPane = loader.load();
			
			MainViewController controller = loader.getController();
			
			Scene scene = new Scene(rootPane);
			Model model = new Model();
			
			controller.setModel(model);
			controller.setStage(primaryStage);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Rucksackproblem Algorithmus");
			primaryStage.setMinHeight(400);
			primaryStage.setMinWidth(600);
			primaryStage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
