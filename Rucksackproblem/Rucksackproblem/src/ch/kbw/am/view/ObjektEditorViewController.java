package ch.kbw.am.view;

import ch.kbw.am.model.Objekt;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ObjektEditorViewController {
	
	private Stage stage;
	private Objekt object;
	private boolean changed = false;
	
	@FXML
	private TextField tfName;
	
	@FXML
	private TextField tfWeight;
	
	@FXML
	private TextField tfValue;
	
	@FXML
	private void save() {
		
		boolean corr = true;
		String name = tfName.getText();
		
		double weight = 0;
		if(tfWeight.getText().matches("^\\d+(?:\\.\\d+)?$"))
			weight = Double.parseDouble(tfWeight.getText());
		else
			corr = false;
		
		double value = 0;
		if(tfValue.getText().matches("^\\d+(?:\\.\\d+)?$"))
			value = Double.parseDouble(tfValue.getText());
		else
			corr = false;
		
		if(corr) {
			if(object == null)
				object = new Objekt(name, weight, value);
			else {
				object.setName(name);
				object.setValue(value);
				object.setWeight(weight);
			}
			changed = true;
			System.out.println("Object saved!");
			stage.close();
			
		}
		else {
			System.out.println("Object not saved!");
			System.out.println("Error: Invalid Input");
		}
		
	}
	
	@FXML
	private void cancel() {
		
		stage.close();
		
	}
	
	public void setStage(Stage s) {
		this.stage = s;
	}
	
	public boolean getChanged() {
		return changed;
	}
	
	public Objekt getObject() {
		return object;
	}
	
	public void setObjekt(Objekt o) {
		this.object = o;
		
		tfName.setText(o.getName());
		tfWeight.setText(""+o.getWeight());
		tfValue.setText(""+o.getValue());
		
	}

}
