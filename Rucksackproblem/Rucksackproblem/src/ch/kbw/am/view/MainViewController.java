package ch.kbw.am.view;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Model;
import ch.kbw.am.model.Objekt;
import ch.kbw.am.model.ObjektCombination;
import ch.kbw.am.model.util.ObjektComparator;
import ch.kbw.am.model.wrapper.ObjektListWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainViewController {
	
	//Program related
	private Model model;
	private int counter = 0;
	private Stage myStage;
	private CalculationData activeCd;
	
	//View related
	@FXML
	private TabPane tabPane;
	
	//Input
	@FXML 
	private TextField tfInputMaxWeight;
	
	@FXML	
	private ListView<String> lvInputAlgos;
	
	@FXML
	private TableView<Objekt> tvInputObjects;
	
	@FXML
	private Label lbInputErrors;
	
	//Output
	@FXML
	private TableView<ObjektCombination> tvOutputObjektCombi;
	
	@FXML
	private Button bDisplayObjekt;
	
	@FXML
	private ScatterChart<Double, Double> scOutputCombinations;
	
	@FXML
	private CheckBox cbAllCombos;
	
	@FXML
	private ChoiceBox<String> cbMapping;
	
	@FXML
	private Label lbOutputCombination;
	
	@FXML
	private Label lbOutputWeight;
	
	@FXML
	private Label lbOutputValue;
	
	@FXML
	private Label lbOutputMaxWeight;
	
	@FXML
	private Label lbOutputNObjects;
	
	@FXML
	private Label lbOutputNCombis;
	
	@FXML
	private Label lbOutputTime;
	
	
	
	//View Initialize
	@FXML
	private void initialize() {
		System.out.println("Initializing Gui ...");
		
		tvInputObjects.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
		tvInputObjects.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("value"));
		tvInputObjects.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("weight"));
		tvInputObjects.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("profit"));
		
		tvOutputObjektCombi.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("rank"));
		tvOutputObjektCombi.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("combination"));
		tvOutputObjektCombi.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("totalValue"));
		tvOutputObjektCombi.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("totalWeight"));
		
		cbMapping.getItems().add("List View");
		cbMapping.getItems().add("Chart View");
		cbMapping.valueProperty().addListener((observable, oldVal, newVal) -> {
			switch(newVal) {
				case "List View":
					displayTable();
					break;
				case "Chart View":
					displayChart();
					break;
				}
		});
		cbMapping.getSelectionModel().select(0);
		
		cbAllCombos.selectedProperty().set(true);
		cbAllCombos.selectedProperty().addListener((observable, oldVal, newVal) -> {
			tvOutputObjektCombi.getItems().clear();
			if(activeCd != null) {
				if(newVal.booleanValue()) {
					for (ObjektCombination oc : activeCd.getCombinations()) {
						tvOutputObjektCombi.getItems().add(oc);
					}
				}
				else {
					for (ObjektCombination oc : activeCd.getCombinations()) {
						if(oc.getRank() > 0)
							tvOutputObjektCombi.getItems().add(oc);
					}
				}
			}
			tvOutputObjektCombi.refresh();
		});
		
	}
	
	//View Input Functions
	@FXML
	private void calculate() {
		
		CalculationData cd = createCalcData();		
		model.addData(cd);
		
		try {
			
			cd.calculate();
			
			lbInputErrors.setText("No Errors");
			System.out.println("Calculation is done ... displaying infos on Ergebnis Tab");
			
			clearOutput();
			displayOutput(cd);
			
			System.out.println("Finished!");
			
			
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			lbInputErrors.setText(e.getMessage());
			e.printStackTrace();
			model.getCalculationDataList().remove(cd);
			
		}
		
		
	}
	
	//tvInputObjekts Buttons
	@FXML
	private void createObject() {
		
		try {
			
			Stage secondaryStage = new Stage();
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ObjektEditorView.fxml"));
			
			BorderPane root = loader.load();
			ObjektEditorViewController controller = loader.getController();
			
			controller.setStage(secondaryStage);
			
			Scene scene = new Scene(root);
			secondaryStage.setScene(scene);
			secondaryStage.setTitle("Objekt Editor");
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			secondaryStage.showAndWait();
			
			if(controller.getChanged())
				tvInputObjects.getItems().add(controller.getObject());
			else
				System.out.println("Object changes have been discarded");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@FXML
	private void editObject() {
		
		Objekt o = tvInputObjects.getSelectionModel().getSelectedItem();
		
		if(o != null ) {
			try {
			
				Stage secondaryStage = new Stage();
			
				FXMLLoader loader = new FXMLLoader(getClass().getResource("ObjektEditorView.fxml"));
				
				BorderPane root = loader.load();
				ObjektEditorViewController controller = loader.getController();
				
				controller.setStage(secondaryStage);
				controller.setObjekt(o);
				
				Scene scene = new Scene(root);
				secondaryStage.setScene(scene);
				secondaryStage.setTitle("Objekt Editor");
				secondaryStage.initModality(Modality.APPLICATION_MODAL);
				secondaryStage.showAndWait();
				
				if(controller.getChanged())
					tvInputObjects.refresh();
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		else
			System.out.println("On tvObjectsInput: No Object selected!");
		
		
		
	}
	
	@FXML
	private void deleteObject() {
		
		int index = tvInputObjects.getSelectionModel().getSelectedIndex();
		
		if(index >= 0)
			tvInputObjects.getItems().remove(index);
		else
			System.out.println("On tvObjectsInput: No Object selected!");
		
	}
	
	//Import / Output Objekts
	@FXML
	private void exportObjektList() {
		
		File file = null;
		
		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML Files (*.xml, arg1)", "*.xml");
		chooser.getExtensionFilters().add(extFilter);
		chooser.setInitialDirectory(new File("\\"));
		
		file = chooser.showSaveDialog(myStage);
		
		if(file != null) {
			try {
				
				JAXBContext context = JAXBContext.newInstance(ObjektListWrapper.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				
				ObjektListWrapper wrapper = new ObjektListWrapper();
				wrapper.setObjektList(tvInputObjects.getItems());
				
				m.marshal(wrapper, file);
				
				
			} catch (Exception e ) {
				e.printStackTrace();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("File Error");
				alert.setContentText("Something went wrong!");
				alert.showAndWait();
			}
		}
	}
	
	@FXML
	private void importObjektList() {
		
		File file = null;
		
		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML Files (*.xml, arg1)", "*.xml");
		chooser.getExtensionFilters().add(extFilter);
		chooser.setInitialDirectory(new File("\\"));
		
		file = chooser.showOpenDialog(myStage);
		
		if(file != null) {
			try {
				
				JAXBContext context = JAXBContext.newInstance(ObjektListWrapper.class);
				Unmarshaller m = context.createUnmarshaller();
				
				ObjektListWrapper wrapper = (ObjektListWrapper) m.unmarshal(file);
				
				tvInputObjects.getItems().clear();
				tvInputObjects.getItems().addAll(wrapper.getObjektList());
				
				
			} catch (Exception e ) {
				e.printStackTrace();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("File Error");
				alert.setContentText("Das eingegebene File konnte nicht gelesen werden! Stellen sie sicher, dass sie das richtige"
						+ " Dokument ge�ffnet haben. ");
				alert.showAndWait();
			}
		}
	}
	
	
	//View Function Output
	private void displayOutput(CalculationData cd) {
		
		tabPane.getSelectionModel().selectNext();
		activeCd = cd;
		displayTable();
		
		//Combination List
		System.out.println("Adding Combinations to Table ..");
		for (ObjektCombination oc: cd.getCombinations()) {
			tvOutputObjektCombi.getItems().add(oc);
		}
		
		System.out.println("Adding CalcData to Scene ..");
		lbOutputCombination.setText(tvOutputObjektCombi.getItems().get(0).getCombination());
		lbOutputValue.setText("" + tvOutputObjektCombi.getItems().get(0).getTotalValue());
		lbOutputWeight.setText("" + tvOutputObjektCombi.getItems().get(0).getTotalWeight());
		
		lbOutputMaxWeight.setText("" + cd.getMaxWeight());
		lbOutputNObjects.setText("" + cd.getNObjects());
		lbOutputNCombis.setText("" + tvOutputObjektCombi.getItems().size());
		lbOutputTime.setText(String.format("%.4g%n", cd.getTime()));
		
		//Combination Chart
		if(!cd.getAlgoName().equals("Bruteforce")) { //Bei zuvielen Objekten funktioniert dies NICHT (deshalb kein Bruteforce zulassen)
			System.out.println("Adding Combinations to Chart ..");
			Series<Double, Double> series = new XYChart.Series<>();
			series.setName("Combinations");
			for (ObjektCombination oc : cd.getCombinations()) {
				series.getData().add(new Data<Double, Double>(oc.getTotalWeight(), oc.getTotalValue()));
			}
			
			
			scOutputCombinations.getData().add(series);
		}
		
		
	}
	
	private void clearOutput() {
		tvOutputObjektCombi.getItems().clear();
		scOutputCombinations.getData().clear();
		
		lbOutputCombination.setText("");
		lbOutputValue.setText("");
		lbOutputWeight.setText("");
		
		lbOutputMaxWeight.setText("");
		lbOutputNObjects.setText("");
		lbOutputNCombis.setText("");
		lbOutputTime.setText("");
		
		cbAllCombos.setSelected(true);
		cbMapping.getSelectionModel().select(0);
		
	}
	
	private void displayTable() {
		scOutputCombinations.setVisible(false);
		
		tvOutputObjektCombi.setVisible(true);
		bDisplayObjekt.setVisible(true);
		cbAllCombos.setVisible(true);
		
	}
	
	private void displayChart() {
		scOutputCombinations.setVisible(true);
		
		tvOutputObjektCombi.setVisible(false);
		bDisplayObjekt.setVisible(false);
		cbAllCombos.setVisible(false);
		
	}
	
	//Call ObjektCombination Inspector
	@FXML
	private void callObjektCombinationInspector() {
		
		ObjektCombination oc = tvOutputObjektCombi.getSelectionModel().getSelectedItem();
		
		if(oc != null) {
			try {
				
				Stage secondaryStage = new Stage();
				
				FXMLLoader loader = new FXMLLoader(getClass().getResource("CombinationInspectorView.fxml"));
				
				BorderPane root = loader.load();
				CombinationInspectorViewController controller = loader.getController();
				
				controller.setStage(secondaryStage);
				controller.setCd(activeCd);
				controller.setOc(oc);
				
				Scene scene = new Scene(root);
				secondaryStage.setScene(scene);
				secondaryStage.setTitle("Combination Inspector");
				secondaryStage.initModality(Modality.APPLICATION_MODAL);
				secondaryStage.showAndWait();
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
	}
	
	//Call other Calculation Datas
	@FXML
	private void callCalculationDataList() {
		try {
			
			Stage secondaryStage = new Stage();
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("CalculationDataSelectorView.fxml"));
			
			BorderPane root = loader.load();
			CalculationDataSelectorViewController controller = loader.getController();
			
			controller.setModel(model);
			controller.setStage(secondaryStage);
			
			Scene scene = new Scene(root);
			secondaryStage.setScene(scene);
			secondaryStage.setTitle("CalculationData List");
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			secondaryStage.showAndWait();
			
			if(controller.getCdSelected() != null) {
				activeCd = controller.getCdSelected();
				clearOutput();
				displayOutput(activeCd);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	//Program Functions
	private CalculationData createCalcData() {
		
		String algoType = lvInputAlgos.getSelectionModel().getSelectedItem();
		double maxWeight = getMaxWeight();
		CalculationData cd = new CalculationData("Dataset#" + counter, algoType, maxWeight, model);
		
		for (Objekt objekt : tvInputObjects.getItems()) {
			cd.addObjects(objekt);
		}
		
		cd.getObjects().sort(new ObjektComparator());
		
		counter++;
		
		return cd;
		
	}
	
	//Get Max weight and check if correct
	private double getMaxWeight() {
		
		double d = 0;
		String s = tfInputMaxWeight.getText();
		
		if(s.matches("^\\d+(?:\\.\\d+)?$"))
			d = Double.parseDouble(s);
		return d;
	}
	
	//Set model and corresponding Data
	public void setModel(Model m) {
		model = m;
		
		for(String s : model.getAlgoTypesName()) {
			lvInputAlgos.itemsProperty().get().add(s);
		}
	}
	
	//Setting Stage
	public void setStage(Stage s) {
		myStage = s;
	}
	
	//Menu Bar Commands FILE
	@FXML
	private void menuClose() {
		myStage.close();
	}
	
	//Menu Bar Commands EDIT
	
	@FXML
	private void menuReset() {
		
		tfInputMaxWeight.clear();
		lvInputAlgos.getSelectionModel().clearSelection();
		tvInputObjects.getItems().clear();
		lbInputErrors.setText("No Errors");
		
		activeCd = null;
		
		clearOutput();
		
	}
	
	@FXML
	private void menuInsert() {
		
		clearOutput();
		
		tvInputObjects.getItems().add(new Objekt("Nr 1", 153, 232));
		tvInputObjects.getItems().add(new Objekt("Nr 2", 54, 73));
		tvInputObjects.getItems().add(new Objekt("Nr 3", 191, 201));
		tvInputObjects.getItems().add(new Objekt("Nr 4", 66, 50));
		tvInputObjects.getItems().add(new Objekt("Nr 5", 239, 141));
		tvInputObjects.getItems().add(new Objekt("Nr 6", 137, 79));
		tvInputObjects.getItems().add(new Objekt("Nr 7", 148, 48));
		tvInputObjects.getItems().add(new Objekt("Nr 8", 249, 38));
		
		tfInputMaxWeight.setText("" + 645);
		
	}
	
	//Menu Bar Commands HELP
	
	@FXML
	private void menuAboutUs() {
		Alert a = new Alert(AlertType.INFORMATION);
		a.setTitle("Hello there");
		a.setContentText("This Program has been planned by Ilario, Benedikt and Yves\n"
				+ "It has been implemented by Yves Meyer\n"
				+ "We hope you enjoy it");
		a.showAndWait();
	}
	
	

}
