package ch.kbw.am.view;

import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Objekt;
import ch.kbw.am.model.ObjektCombination;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class CombinationInspectorViewController {
	
	//Program Vars
	private Stage myStage;
	private CalculationData cd;
	private ObjektCombination oc;
	
	//View Vars
	@FXML
	private Label lbRank;
	
	@FXML
	private Label lbCombination;
	
	@FXML
	private Label lbWeight;
	
	@FXML
	private Label lbValue;
	
	@FXML
	private TableView<Objekt> tvObjekts;
	
	//initalize
	@FXML
	private void initialize() {
		
		tvObjekts.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
		tvObjekts.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("value"));
		tvObjekts.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("weight"));
		
	}
	
	//View Functions
	@FXML
	private void close() {
		myStage.close();
	}
	
	
	//Program Functions
	public void setStage(Stage s) {
		myStage = s;
	}
	
	public void setOc(ObjektCombination c) {
		oc = c;
		
		displayData();
	}
	
	public void setCd(CalculationData c) {
		cd = c;
		
		displayData();
	}
	
	//Extra Function made, so sequence of setOc or setCd doesnt matter
	public void displayData() {
		
		if(oc != null && cd != null) {
			lbRank.setText("" + oc.getRank());
			lbCombination.setText("" + oc.getCombination());
			lbWeight.setText("" + oc.getTotalWeight());
			lbValue.setText("" + oc.getTotalValue());
			
			for (int i = 0; i < cd.getObjects().size(); i++) {
				if(oc.getCombination().charAt(i) == '1')
					tvObjekts.getItems().add(cd.getObjects().get(i));
			}
		}
		
	}

}
