package ch.kbw.am.view;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ch.kbw.am.model.CalculationData;
import ch.kbw.am.model.Model;
import ch.kbw.am.model.wrapper.CalculationDataListWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class CalculationDataSelectorViewController {
	
	private Stage myStage;
	private Model myModel;
	private CalculationData cdSelected;
	
	//View Variables
	@FXML
	private TableView<CalculationData> tvCalculationData;
	
	//Initialize
	@FXML
	private void initialize() {
		
		tvCalculationData.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
		tvCalculationData.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("algoName"));
		tvCalculationData.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("nObjects"));
		tvCalculationData.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("maxWeight"));
		
	}
	
	//View Functions
	@FXML
	private void cancel() {
		myStage.close();
	}
	
	@FXML
	private void select() {
		
		cdSelected = tvCalculationData.getSelectionModel().getSelectedItem();
		if(cdSelected != null) {
			myStage.close();
		}
		
	}
	
	//Import und Export
	@FXML
	private void exportObjektList() {
		
		CalculationData selectedCd = tvCalculationData.getSelectionModel().getSelectedItem();
		
		if(selectedCd != null) {
			File file = null;
			FileChooser chooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML Files (*.xml, arg1)", "*.xml");
			chooser.getExtensionFilters().add(extFilter);
			chooser.setInitialDirectory(new File("\\"));
			
			file = chooser.showSaveDialog(myStage);
			
			if(file != null) {
				try {
					
					JAXBContext context = JAXBContext.newInstance(CalculationDataListWrapper.class);
					Marshaller m = context.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					
					CalculationDataListWrapper wrapper = new CalculationDataListWrapper();
					//Creating Observable List for wrapper
					ArrayList<CalculationData> alCd = new ArrayList<>();
					alCd.add(selectedCd);
					wrapper.setObjektList(alCd);
					
					m.marshal(wrapper, file);
					
				} catch (Exception e ) {
					e.printStackTrace();
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("File Error");
					alert.setContentText("Something went wrong!");
					alert.showAndWait();
				}
			}
		}
		
	}
	
	@FXML
	private void importObjektList() {
		
		File file = null;
		
		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML Files (*.xml, arg1)", "*.xml");
		
		chooser.getExtensionFilters().add(extFilter);
		chooser.setInitialDirectory(new File("\\"));
		
		file = chooser.showOpenDialog(myStage);
		
		
		if(file != null) {
			try {
				
				JAXBContext context = JAXBContext.newInstance(CalculationDataListWrapper.class);
				Unmarshaller m = context.createUnmarshaller();
				
				CalculationDataListWrapper wrapper = (CalculationDataListWrapper) m.unmarshal(file);
				
				myModel.addData(wrapper.getObjektList().get(0)); //Nimmt das 0te da die "Liste" imagin�r ist .. es wird immer nur ein Objekt gespeichert sein
				tvCalculationData.getItems().clear();
				tvCalculationData.getItems().addAll(myModel.getCalculationDataList());
				tvCalculationData.refresh();
				
			} catch (Exception e ) {
				e.printStackTrace();
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("File Error");
				alert.setContentText("Das eingegebene File konnte nicht gelesen werden! Stellen sie sicher, dass sie das richtige"
						+ " Dokument ge�ffnet haben. ");
				alert.showAndWait();
			}
		}
	}
	
	//Program functions
	public void setStage(Stage s) {
		myStage = s;
	}
	
	public void setModel(Model m) {
		myModel = m;
		
		tvCalculationData.getItems().addAll(myModel.getCalculationDataList());
	}
	
	public CalculationData getCdSelected() {
		return cdSelected;
	}
	
	//Import export
	
	

}
